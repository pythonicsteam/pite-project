from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from clientApp.views import HomePageView, register
from account.views import UserDetail
from game.views import GameList, GameDetail, UserGameList, gameOrder, gameRate

urlpatterns = [

    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url('^', include('django.contrib.auth.urls')),

    # User views
    url(r'^accounts/profile/$', UserDetail.as_view(), name='accounts_profile'),
    url(r'^register/$', register, name='register'),
    # Game views
    url(r'^games/$', GameList.as_view(), name='game_list'),
    url(r'^mygames/$', login_required(UserGameList.as_view()), name='user_game_list'),
    url(r'^games/(?P<pk>[0-9]+)/$', GameDetail.as_view(), name='game_detail'),
    url(r'^gameOrder/$', gameOrder, name='game_order'),
    url(r'^gameRate/$', gameRate, name='game_rate'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
]
