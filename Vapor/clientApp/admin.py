from django.contrib import admin
from django import forms
from dbModels.models import Game, GameTitle, Platform, CPU, GPU, RequiredRam, Genre, Tag, GameHasTag, GameMinCPU, GameRecommendedCPU, GameMinGPU, GameRecommendedGPU, Vendor, GameStudio, GameWasReleased, Sale
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
import re


# Register your models here.
class UserForm(forms.ModelForm):
	def clean(self):
		firstName = self.cleaned_data.get('first_name', '')
		lastName = self.cleaned_data.get('last_name', '')
		user_validator = re.compile(u'^\w+$', re.U)

		if not user_validator.match(firstName):
			raise ValidationError('Firstname can conatin only letters and digits!')

		if not user_validator.match(lastName):
			raise ValidationError('Lastname can conatin only letters and digits!')				

	class Meta:
		model = User
		fields = ['username', 'password', 'first_name', 'last_name', 'email', 'groups', 'user_permissions', 'date_joined']

class SaleForm(forms.ModelForm):
	class Meta:
		model = Sale
		fields = ['start_date', 'end_date', 'factor']

class UserAdmin(admin.ModelAdmin):
	form = UserForm

class GameHasTagInline(admin.TabularInline):
	model = GameHasTag

class TagAdmin(admin.ModelAdmin):
	inlines = (GameHasTagInline, )

class GameWasReleasedInline(admin.TabularInline):
	model = GameWasReleased

class GameStudioAdmin(admin.ModelAdmin):
	inlines = (GameWasReleasedInline, )

class GameMinCPUInline(admin.TabularInline):
	model = GameMinCPU

class GameRecommendedCPUInline(admin.TabularInline):
	model = GameRecommendedCPU

class CPUAdmin(admin.ModelAdmin):
	inlines = (GameMinCPUInline, GameRecommendedCPUInline)

class GameMinGPUInline(admin.TabularInline):
	model = GameMinGPU

class GameRecommendedGPUInline(admin.TabularInline):
	model = GameRecommendedGPU

class GPUAdmin(admin.ModelAdmin):
	inlines = (GameMinGPUInline, GameRecommendedGPUInline)

class RequiredRamInline(admin.TabularInline):
	model = RequiredRam

class SaleInline(admin.TabularInline):
	model = Sale
	form = SaleForm

class GameTitleInline(admin.TabularInline):
	model = GameTitle

class GameTitleAdmin(admin.ModelAdmin):
	inlines = (GameHasTagInline, GameWasReleasedInline)

class GenreAdmin(admin.ModelAdmin):
	inlines = (GameTitleInline, )

class GameAdmin(admin.ModelAdmin):
	inlines = (SaleInline, GameMinCPUInline, GameRecommendedCPUInline, GameMinGPUInline, GameRecommendedGPUInline, RequiredRamInline)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(GameTitle, GameTitleAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Platform)
admin.site.register(Tag, TagAdmin)
admin.site.register(GameStudio, GameStudioAdmin)
admin.site.register(CPU, CPUAdmin)
admin.site.register(GPU, GPUAdmin)
admin.site.register(RequiredRam)
admin.site.register(Sale)
admin.site.register(Vendor)
