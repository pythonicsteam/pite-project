from django.shortcuts import render
from django.views.generic import DetailView
from dbModels.models import User


class UserDetail(DetailView):

    template_name = 'account/user_detail.html'

    def get_object(self, queryset=None):
        return self.request.user
