# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Comments',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('content', models.CharField(max_length=2000)),
            ],
        ),
        migrations.CreateModel(
            name='CommentsArchive',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('comment_id', models.IntegerField()),
                ('date', models.DateTimeField()),
                ('modification_date', models.DateTimeField()),
                ('content', models.CharField(max_length=2000)),
            ],
        ),
        migrations.CreateModel(
            name='CPU',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('release_date', models.DateTimeField()),
                ('price', models.DecimalField(max_digits=10, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='GameHasTag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='GameMinCPU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cpu', models.ForeignKey(to='dbModels.CPU')),
                ('game', models.ForeignKey(to='dbModels.Game')),
            ],
        ),
        migrations.CreateModel(
            name='GameMinGPU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('game', models.ForeignKey(to='dbModels.Game')),
            ],
        ),
        migrations.CreateModel(
            name='GameRecommendedCPU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cpu', models.ForeignKey(to='dbModels.CPU')),
                ('game', models.ForeignKey(to='dbModels.Game')),
            ],
        ),
        migrations.CreateModel(
            name='GameRecommendedGPU',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('game', models.ForeignKey(to='dbModels.Game')),
            ],
        ),
        migrations.CreateModel(
            name='GameStudio',
            fields=[
                ('name', models.CharField(max_length=100, serialize=False, primary_key=True)),
                ('synopsis', models.CharField(max_length=1000)),
                ('url', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='GameTitle',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('synopsis', models.CharField(max_length=1000)),
            ],
        ),
        migrations.CreateModel(
            name='GameWasReleased',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('game_title', models.ForeignKey(to='dbModels.GameTitle')),
                ('studio', models.ForeignKey(to='dbModels.GameStudio')),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='GPU',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Orders',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('date', models.DateTimeField()),
                ('price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('game', models.ForeignKey(to='dbModels.Game')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Platform',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', models.IntegerField()),
                ('game', models.ForeignKey(to='dbModels.Game')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='RequiredRam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('minimum', models.IntegerField()),
                ('recommended', models.IntegerField()),
                ('game', models.ForeignKey(to='dbModels.Game', unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Sale',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('factor', models.FloatField()),
                ('game', models.ForeignKey(to='dbModels.Game')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True)),
            ],
        ),
        migrations.AddField(
            model_name='gpu',
            name='vendor',
            field=models.ForeignKey(to='dbModels.Vendor'),
        ),
        migrations.AddField(
            model_name='gametitle',
            name='genre_name',
            field=models.ForeignKey(to='dbModels.Genre'),
        ),
        migrations.AddField(
            model_name='gametitle',
            name='studio',
            field=models.ManyToManyField(to='dbModels.GameStudio', through='dbModels.GameWasReleased'),
        ),
        migrations.AddField(
            model_name='gametitle',
            name='tag',
            field=models.ManyToManyField(to='dbModels.Tag', through='dbModels.GameHasTag'),
        ),
        migrations.AddField(
            model_name='gamerecommendedgpu',
            name='gpu',
            field=models.ForeignKey(to='dbModels.GPU'),
        ),
        migrations.AddField(
            model_name='gamemingpu',
            name='gpu',
            field=models.ForeignKey(to='dbModels.GPU'),
        ),
        migrations.AddField(
            model_name='gamehastag',
            name='game_title',
            field=models.ForeignKey(to='dbModels.GameTitle'),
        ),
        migrations.AddField(
            model_name='gamehastag',
            name='tag',
            field=models.ForeignKey(to='dbModels.Tag'),
        ),
        migrations.AddField(
            model_name='game',
            name='owners',
            field=models.ManyToManyField(related_name='users_that_owned', through='dbModels.Orders', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='game',
            name='platform',
            field=models.ForeignKey(to='dbModels.Platform'),
        ),
        migrations.AddField(
            model_name='game',
            name='ratings',
            field=models.ManyToManyField(related_name='users_that_rated', through='dbModels.Rating', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='game',
            name='title',
            field=models.ForeignKey(to='dbModels.GameTitle'),
        ),
        migrations.AddField(
            model_name='cpu',
            name='vendor',
            field=models.ForeignKey(to='dbModels.Vendor'),
        ),
        migrations.AddField(
            model_name='comments',
            name='game',
            field=models.ForeignKey(to='dbModels.Game'),
        ),
        migrations.AddField(
            model_name='comments',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='rating',
            unique_together=set([('user', 'game')]),
        ),
        migrations.AlterUniqueTogether(
            name='orders',
            unique_together=set([('user', 'game')]),
        ),
        migrations.AlterUniqueTogether(
            name='gpu',
            unique_together=set([('vendor', 'name')]),
        ),
        migrations.AlterUniqueTogether(
            name='gamewasreleased',
            unique_together=set([('game_title', 'studio')]),
        ),
        migrations.AlterUniqueTogether(
            name='gamerecommendedgpu',
            unique_together=set([('game', 'gpu')]),
        ),
        migrations.AlterUniqueTogether(
            name='gamerecommendedcpu',
            unique_together=set([('game', 'cpu')]),
        ),
        migrations.AlterUniqueTogether(
            name='gamemingpu',
            unique_together=set([('game', 'gpu')]),
        ),
        migrations.AlterUniqueTogether(
            name='gamemincpu',
            unique_together=set([('game', 'cpu')]),
        ),
        migrations.AlterUniqueTogether(
            name='gamehastag',
            unique_together=set([('game_title', 'tag')]),
        ),
        migrations.AlterUniqueTogether(
            name='cpu',
            unique_together=set([('vendor', 'name')]),
        ),
    ]
