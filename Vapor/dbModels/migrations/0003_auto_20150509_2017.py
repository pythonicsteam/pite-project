# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbModels', '0002_gametitle_picture'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requiredram',
            name='minimum',
            field=models.IntegerField(verbose_name=b'Minimum RAM [GB]'),
        ),
        migrations.AlterField(
            model_name='requiredram',
            name='recommended',
            field=models.IntegerField(verbose_name=b'Recommended RAM [GB]'),
        ),
    ]
