# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbModels', '0003_auto_20150509_2017'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='commentsarchive',
            name='date',
        ),
    ]
