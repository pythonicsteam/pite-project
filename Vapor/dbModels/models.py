from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class Genre(models.Model):
	name = models.CharField(primary_key = True, max_length = 50)

	def __unicode__(self):
		return self.name

class Platform(models.Model):
	name = models.CharField(primary_key = True, max_length = 50)

	def __unicode__(self):
		return self.name

class Tag(models.Model):
	name = models.CharField(primary_key = True, max_length = 50)

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['name']

class Vendor(models.Model):
	name = models.CharField(primary_key = True, max_length = 50)

	def __unicode__(self):
		return self.name

class CPU(models.Model):
	id = models.AutoField(primary_key = True)
	vendor = models.ForeignKey(Vendor)
	name = models.CharField(max_length = 50)

	def __unicode__(self):
		return self.name

	class Meta:
		unique_together = ('vendor', 'name')

class GPU(models.Model):
	id = models.AutoField(primary_key = True)
	vendor = models.ForeignKey(Vendor)
	name = models.CharField(max_length = 50)

	def __unicode__(self):
		return self.name

	class Meta:
		unique_together = ('vendor', 'name')

class GameStudio(models.Model):
	name = models.CharField(primary_key = True, max_length = 100)
	synopsis = models.CharField(max_length = 1000)
	url = models.URLField(max_length = 200)

	def __unicode__(self):
		return self.name

class GameTitle(models.Model):
	id = models.AutoField(primary_key = True)
	genre_name = models.ForeignKey(Genre)
	name = models.CharField(max_length = 100)
	synopsis = models.CharField(max_length = 1000)
	picture = models.ImageField(null = True, upload_to = 'games')

	studio = models.ManyToManyField(GameStudio, through = 'GameWasReleased', through_fields = ('game_title', 'studio')) # game was released
	tag = models.ManyToManyField(Tag, through = 'GameHasTag', through_fields = ('game_title', 'tag')) # game has tag

	def __unicode__(self):
		return self.name

class Game(models.Model):
	id = models.AutoField(primary_key = True)
	platform = models.ForeignKey(Platform)
	title = models.ForeignKey(GameTitle)
	release_date = models.DateTimeField()
	price = models.DecimalField(max_digits = 10, decimal_places = 2)

	ratings = models.ManyToManyField(User, through = 'Rating', through_fields = ('game', 'user'), related_name='users_that_rated')
	owners = models.ManyToManyField(User, through = 'Orders', through_fields = ('game', 'user'), related_name='users_that_owned')

	def __unicode__(self):
		return self.title.name

	def clean(self):
		if self.price <= 0:
			raise ValidationError('Price has to be bigger than zero!')

class GameWasReleased(models.Model):
	game_title = models.ForeignKey(GameTitle)
	studio = models.ForeignKey(GameStudio)

	class Meta:
		unique_together = ('game_title', 'studio')

class GameHasTag(models.Model):
	game_title = models.ForeignKey(GameTitle)
	tag = models.ForeignKey(Tag)

	class Meta:
		unique_together = ('game_title', 'tag')

class Rating(models.Model):
	user = models.ForeignKey(User)
	game = models.ForeignKey(Game)
	rating = models.IntegerField()

	class Meta:
		unique_together = ('user', 'game')

	def clean(self):
		if self.rating < 0 or self.rating > 10:
			raise ValidationError('Rating needs to be in a range of 1 to 10.')

class Orders(models.Model):
	id = models.AutoField(primary_key = True)
	user = models.ForeignKey(User)
	game = models.ForeignKey(Game)
	date = models.DateTimeField()
	price = models.DecimalField(max_digits = 10, decimal_places = 2)

	class Meta:
		unique_together = ('user', 'game')

class Comments(models.Model):
	id = models.AutoField(primary_key = True)
	user = models.ForeignKey(User)
	game = models.ForeignKey(Game)
	date = models.DateTimeField()
	content = models.CharField(max_length = 2000)

class CommentsArchive(models.Model):
	id = models.AutoField(primary_key = True)
	comment_id = models.IntegerField(null = False)
	modification_date = models.DateTimeField()
	content = models.CharField(max_length = 2000)

class GameMinCPU(models.Model): 
	game = models.ForeignKey(Game)
	cpu = models.ForeignKey(CPU)

	class Meta:
		unique_together = ('game', 'cpu')

class GameRecommendedCPU(models.Model):
	game = models.ForeignKey(Game)
	cpu = models.ForeignKey(CPU)

	class Meta:
		unique_together = ('game', 'cpu')

class GameMinGPU(models.Model):
	game = models.ForeignKey(Game)
	gpu = models.ForeignKey(GPU)

	class Meta:
		unique_together = ('game', 'gpu')

class GameRecommendedGPU(models.Model): 
	game = models.ForeignKey(Game)
	gpu = models.ForeignKey(GPU)

	class Meta:
		unique_together = ('game', 'gpu')

class RequiredRam(models.Model):
	game = models.ForeignKey(Game, unique = True)
	minimum = models.IntegerField(verbose_name = 'Minimum RAM [GB]') 
	recommended = models.IntegerField(verbose_name = 'Recommended RAM [GB]') 

	def clean(self):
		if self.minimum > self.recommended:
			raise ValidationError('Minimum requirements can not to be higher than recommended requirements!')

	def __unicode__(self):
		return ""

class Sale(models.Model):
	id = models.AutoField(primary_key = True)
	game = models.ForeignKey(Game)
	start_date = models.DateTimeField()
	end_date = models.DateTimeField()
	factor = models.FloatField()

	def __unicode__(self):
		return str(self.game.title)


# Add your models here.
