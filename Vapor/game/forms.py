from django import forms
from django.contrib.auth.models import User
from dbModels.models import Orders, Platform, Genre, Game, Tag
from django.forms import inlineformset_factory


class GameFilterForm(forms.Form):

    PLATFORMS = [(i.name, i.name) for i in Platform.objects.all()]
    PLATFORMS.insert(0, ("", "All"))

    GENRES = [(i.name, i.name) for i in Genre.objects.all()]
    GENRES.insert(0, ("", "All"))

    TAGS = [(i.name, i.name) for i in Tag.objects.all()]
    TAGS.insert(0, ("", "All"))

    title = forms.CharField(required=False)
    genre = forms.ChoiceField(required=False, widget=forms.Select, choices=GENRES)
    platform = forms.ChoiceField(required=False, widget=forms.Select, choices=PLATFORMS)
    tag = forms.ChoiceField(required=False, widget=forms.Select, choices=TAGS)

