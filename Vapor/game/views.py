from django.contrib import messages
from django.http import HttpResponseRedirect
from django.views.generic import ListView, DetailView
from dbModels.models import GameTitle, Orders, Game, Rating
from .forms import GameFilterForm
from datetime import datetime
from django.db.models import Avg


class GameList(ListView):
    template_name = 'game/game_list.html'

    def get_queryset(self):
        titles = GameTitle.objects.all()

        form = GameFilterForm(self.request.GET)
        if form.is_valid():
            title = form.cleaned_data['title']
            genre = form.cleaned_data['genre']
            platform = form.cleaned_data['platform']
            tag = form.cleaned_data['tag']

            if title:
                titles = titles.filter(name__icontains=title)

            if genre:
                titles = titles.filter(genre_name__name=genre)

            if platform:
                titles = titles.filter(game__platform__name=platform)

            if tag:
                titles = titles.filter(tag__name=tag)

        titles = titles.annotate(rating=Avg('game__rating__rating'))

        return titles

    def get_context_data(self, **kwargs):
        context = super(GameList, self).get_context_data(**kwargs)
        context['form'] = GameFilterForm(self.request.GET)

        return context


class GameDetail(DetailView):
    template_name = 'game/game_detail.html'
    model = GameTitle;

#@login_required(login_url='/accounts/login/')
class UserGameList(GameList):
    template_name = 'game/game_list.html'

    # def get_queryset(self):
    #     #titles = super(UserGameList, self).get_queryset().filter(game__owners = self.request.user).distinct()
    #     titles = [i.title for i in Game.objects.filter(owners=self.request.user)]
    #     print titles
    #     return titles

    def get_context_data(self, **kwargs):
        context = super(UserGameList, self).get_context_data(**kwargs)
        titles = super(UserGameList, self).get_queryset().filter(game__owners=self.request.user).distinct()
        context['object_list'] = titles
        context['games'] = Game.objects.filter(owners=self.request.user)

        return context


def gameOrder(request):
   if request.method == 'POST':
        platform = request.POST['platform']
        game = Game.objects.get(id=platform)
        user = request.user
        try:
            order = Orders.objects.get(game = game, user = user)
        except Orders.DoesNotExist:
            order = None
        if order:
            messages.error(request, "You have already bought that game!")
        else:
            order = Orders(game = game, user = user, date = datetime.now(), price = game.price)
            order.save()
            messages.success(request, "Thank you for buying the game! We hope you will enjoy playing!")

        return HttpResponseRedirect('/games/' + str(game.title.id) + '/')
   else:
        return HttpResponseRedirect('/')

def gameRate(request):
   if request.method == 'POST':
        rate = request.POST['rate']
        game = request.POST['game']
        game = Game.objects.get(id=game)
        user = request.user
        try:
            rating = Rating.objects.get(game = game, user = user)
            rating.rating = rate
        except Rating.DoesNotExist:
            rating = Rating(game = game, user = user, rating = rate)
        rating.save();

        return HttpResponseRedirect('/games/' + str(game.title.id) + '/')
   else:
        return HttpResponseRedirect('/')
