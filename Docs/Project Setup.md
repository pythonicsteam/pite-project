# Vapor - Project setup

#### 1. Download and install Python 2.x
* Windows
    * [x86-64 MSI Installer](https://www.python.org/ftp/python/2.7.9/python-2.7.9.amd64.msi)
    * [x86 Installer](https://www.python.org/ftp/python/2.7.9/python-2.7.9.msi)
* Linux  
    Install following packages: **python2.x** and **python2.x-dev**

#### 2. Download and install PostgreSQL
* Windows
    * [x86-64 Installer](http://www.enterprisedb.com/postgresql-941-installers-win64?ls=Crossover&type=Crossover)
    * [X86 Installer](http://www.enterprisedb.com/postgresql-941-installers-win32?ls=Crossover&type=Crossover)
* Linux - install following packages:
    * postgresql-client-9.4
    * postgresql-9.4
    * postgresql-contrib-9.4
    * libpq-dev
    * postgresql-server-dev-9.4
    * pgadmin3 
    
> Urls and packages above assumes using version 9.4 of PostgreSql but version 9.3 also should be fine.

* PostgreSQL installation on Windows  
This is standard installation with two exceptions. Installer ask for password for database admin. That password cannot be lost so be careful when choosing it.

* PostgreSQL installation on Linux  
After installing all packages mentioned earlier run postgres command line interface:

		$ sudo -u postgres psql postgres  
	
	This command will run postgres as user "postgres". Then change password by command:

		$ \password postgres
	
	and enter strong, at least 30 characters password :)

    **Default PostgreSQL database port is 5432.**

* Next steps will be prepared using pgAdmin III
    * Connect to running PostgreSQL database.
    * Right click on database server name select new object and new login role. Set role name and password to "app". Then check all permissions and click OK.
    * Create new database called "djangoDb" and set "app" user as an owner
    
#### 3. Download and install Django  
After installing Python you should have pip (python package manager) installed. Otherwise check [this](https://pip.pypa.io/en/latest/installing.html#install-pip)
On Windows you can find pip in the python installation folder.
When the pip is installed run the following command:

    $ pip install Django 
    
> On Linux without virtualenv for python command must be run as a root

Now you should have Django installed. Next clone project repository from this site.

> Admin site bootstrap css

In order to boostrap css work correctly with django admin sites you have to install django-admin-bootstrapped package. 
To do this run the following command:

    $ pip install django-admin-bootstrapped
    
Now admin sites should looks pretty!

To connect Django with PostgreSQL database _psycopg2_ library with version higher than 2.4.5 is needed.
Windows installers for this library can be found [here](http://stickpeople.com/projects/python/win-psycopg/). On Linux it should be available in repositories.

Vapor project is configured to connect do "djangoDb" database on port 5432. User and password is "app". 
If database configuration have been prepared as mentioned earlier then no changes in settings.py are required.

Now it's time to run Django database migrations to create database structure.
In project folder run following command:

    $ python manage.py migrate
    
If earlier configuration should been prepared correctly, Django creates tables in database.

Now it should be possible to run django server with following command:

    $ python manage.py runserver 8080
    
Site is accessible at [http://localhost:8080](http://localhost:8080)

## Happy coding!



		
