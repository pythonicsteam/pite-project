# Dear Sirs or Madams,

The time has come to pass our work and our ideas to you. 
We would like to leave this note to ease the transition.
So, for one:

## we would like to present the idea, that stood behind Vapor project. 

We figured out that we'd like to make a Steam-like portal, in order to connect people with games (and earn money ;)). 
The idea was, that the portal of sort should list games: and what are games? They are objects with titles, prices. They are developped by some studios, 
they're available on some platforms... We though that there should be possibility to share some info about them in comments and rating - so there would 
be more 'social activities' between users. We build whole project on those ideas, and we've left you with complete database model (as a picture, 
DbDesignerFork xml and, ofcourse, django models). For the sake of consistency we chose to made separate django app for database (called 
extremely originally "dbModels"). You're welcome to separate those if you don't like the idea, but it has proven quite ok for us.

## We would also like to say what we did achieve.

As mentioned before, we've created database model and provided initial json data to populate it. We though carefully about the model and whole project - 
we hope that you'll find our ideas and solutions reasonable, and that documentation provided by us (especially Project Setup manual) will help you to
start your part of the journey with Vapor project.

But, to the **point**. Functionalities that are done/are yet to be are listed in PlannedFunctionalities document. 
We have covered user management: registration, password recovery, administrative panel. We have two e-mail accounts that were used: 
pitevapor[at]gmail.com that is used to send password recovery tokens (login data is in settings.py in Vapor app - if you'll figure out safer way to 
store it, please, don't hesitate ;)) and testuser[at]interia.eu, that has been used for two Vapor user accounts (TestUser and Admin). As we are to 
store 'everything' regarding project in repository - remember phrase krowa1 and use it wisely ;) Or change it, if that does not suit you.

We prepared game listing and information about one particular game, we also covered functionality of buying games (for a specific platform, ofcourse). 
Every user can also view games that he obtained. Also rating is done - and the user can always change his/hers mind about rating ;) We though that it
would be good to be able to separately rate every available platform - as a game that is amazingly done for PC can be completely messed up on Xbox 
(controls, for one might be a good example of what might go wrong).

## And finally: we'd like to show you what is yet to be done.

Thera are still some things that need to be done. For one: advanced search. We populated the database with some requirements for PC games. It would be 
good to use those to search for games that will work with some particullar hardware (GPU, CPU or RAM). Also some filtering by price or game studio that 
released the game would be apreciated.

Secondly, we have left you in charge of implementation of comments mechanism - we assumed that every (logged) user will have an opportunity to leave 
unlimited number of comments that will be connected to the game, and that Administrator will be able to change those that will be deemed 'inapropiate'. 
For sake of security, all the changes supposedly should be stored in CommentsArchive table in database, but it's up to you how you'll resolve that ;)

Another fancy functionality we thought about are sales. The idea is to change the price for a particular game by a factor (factor of 2, as twice the
price is also possible :P) for some period of time. The information about current/incoming sales might be shown on the main page as some kind of ad.

Finally, from the administrative point of view, there should be a way to generate some reports - about amount of money gained in some period, some
user activities (comments or ratings) by game, so on, so on. We'll leave specifics to your consideration ;)

### Please, take good care of our legacy ;)

### Yours Faithfully, 
### Pythonics Team

PS. At the beggining of the planning phase, not knowing how working with Django would look we came up with CodeGuidelines ideas, that did not work out 
that well. After learning how things are build in Django we tried to follow some Django-naming schemes (like with models) but it's not perfect. Also, we 
had some problems with figuring out how to test things (as unit-tests, we were performing integration testing), so you're welcome to try adding some ;)