# Vapor project planned functionality

Summary of planned functionality for Vapor: web-accesible game shop. 
This is more conceptual than formal document but it shall be a guide for developers.

Functionalities that are yet to be implemented are in bold.

## User functionalities
* registration
* login
* listing all games with summarized info (+ various filters!)
* presenting one game with full info, options etc.
* listing account bind ('bought') games per logged user
* 'buy now' + nice screen with 'cd-key'
* **search engine with various filters:**
	- by title
	- by platform
	- **by game studio**
	- **by price**
	- by genre
* **search by platform & requirements**
* rating mechanism (and **rating for game calculated separately from players owning the game and all the ratings**)
* **comments mechanism**
* **showing game studio information**

## Management functionalities
* login
* user management
* addition/edition of 'simple' database content (vendors, CPUs, genres etc.)
* addition/edition of 'games' as multi-table entities
* **discounts/sale**
* **generating reports:**
	- **income in period of time**
	- **popularity reports + filters**
	- **user activities reports (comments, ratings, buying)**
	- **various other statistics + filters**
* **moderation of comments:**
	- **displaying list of those + filters**
	- **keeping track of original comments in separate table** 

