***
# Meetings journal
***
## Meeting 1. April 7, 2015
The aim of the first meeting was to discuss the main concept of the project, arrange roles for Team Memers and establish grand rules for developing and co-working.
### Appointment Secretary (Scribe)
The first decision made during the disscusion was choosing the Team's Secretary. He will be responsible for taking short notes during every meeting focising on what has been decided, who is responsible for doing what, who did and who didn't do his part of the job. This role was given to Tadek.
### Repository Structure
Sources will be stored in 3 basic parts:

*docs - includes all manuals,  code guidelines, meeting journal, etc.

*sorurce - includes the main part of the project source code of an application

*tests - includes all tests written for an application

### Project's topic and technologies
The chosen topic for the project is the Inventory. It will be based on Django platform, running on PostgreSQL database. System administrator's appliation may use Qt for non-browser access, but it will be decided later.

###Code guidelines
Last issue disscusses was the code guidelines. The basic rules was created by Monika. It gathered all standards we will follow during code development.
Those standards are presented in separate document. We reached the agreement very fast, being taught to program in similar ways. The only one issue disscussed longer time was way of declaring fields of classes.

###Next appointment
The second appointment was settled for April 21, which is after following classes and when we would have feedback from client(lecturer).
***
## Meeting 2. April 12, 2015
Facing the fact that we had to present the results of arrangement earlier than planned we decided to organize a conference call on Sunday after the Summa Mass in church.
###Environment
Konrad installed the test local Django system for he project, connected it to the SQL server and tested its functions. Having heard his promissing results we decided to give Konrad the role of an Environment Engineer. 
###Public variables
Despite Python's  variables accessability we wanted to standarize the way of accessing them. After short disscussion we settled to encapsulate all variables we want have acces from the outside of the class with get and set function.
###Topic and technologies
Both client and administrator (owner) parts of the application will be written in Django. The idea of writting sepatate desktop application for owner was left. Django seemed to be a better approach because of the object-relation mapping built in the framework. Writing separate part in Qt we would be forced to develop some functions twice.
###Tests
Last role was given to Marcin. He is the Quality Assurance Engineer. His main responsibilities will be making sure all parts of the code are tesed (by appropriate developers, not necessarily by Marcin himself) and identyfing all errors and bugs to bee fixed by us and the team who will take control on the project at the end of May. 
###Next meeting
Next meeting was postponed on Wednesday April 15, after having feedback from client(lecturer).
***
## Meeting 3. April 15, 2015
Having recived a feedback from lecturer, it's time to establish the basic functionalities of the application. 
###Functionalities
We disscused what logged user will be able to do with our application:
Besides obvious searching and buying, we decided to allow user to comment on games, rate games on every hardware platform and others. The whole concept is presented in separate document.
###Database model
The data will be stored in database which most important parts are tables:
*games - stores informations about specific game on different platforms
*users - informations about buyers
*cpu, graphics_card - minimum and recomended requirements.
Entity-Relation Diagram is presented in separate document.
###Developing environment
To have unified local development environment, Konrad by 19th April will create a Django setup manual for all of us.
###User interface
Next week we will be disscusing the User Interface so Tadek will draw some basic concept of the front side.
###Next meeting
Next meeting was postponed on Wednesday April 22.
***
##Meeting 4. April 22, 2015
The main subject of the meeting is to create and run basic functionalities to our project.
###Seting up the project
Konrad created Django setup manual for us for creating Django, so we can start developing application. We ensured that everyone has the project set, so we can all start coding.
We decided to start with creating some basic funcionalities for the project like:
 
 * Login and logout 
 
 * Administration panel - users' account management
 
 * User's menu - showing details about user
 
 * Password changing
 
 * Registering a new user
 
Konrad will create the administrator menu and management, Tadek will make user registration and signing in. Monika will provide initial database and Marcin will manage constraints in database's tables.
***
##Meeting 5. April 29, 2015
After last meeting we created basic functionalities for user and management. The system lets us to register, log in and show basic informations about users. To make it look decent, we provided bootstrap css library and font-awesome icons.
###New functionalities
Our next week asssignments will be:
 
 * Showing list of games - up to Konrad 
 
 * Showing informations about single game - Tadek
 
 * Creating sollution for storing pictures for game in database - Monika
 
 * Creating relations in GamePlatform part of database - Marcin

Next meeting should be announced after 7th of May after feedback from Client(Lecturer).
***
##Meeting 6. May 6, 2015
Facing the fact that we have to give the project to the next group this week's appointment focused on assigning tasks to do this week.
###New functionalities
The asssignments are:
 
 * Make logout function use bootstrap interface - up to Konrad
 
 * Reseting password by sending email - Konrad
 
 * Enabling user to add name details while registering - Tadek
 
 * Creating buying games and ratings functionalities - Tadek
 
 * Administrator panel, using CSS - Marcin

***

##Last week of project. May, 11-17 
Last meeting we had no traditional meeting. Instead, we had constatnt remote information exchange, about solved tasks.
We did our best to develop project to appropriate form to exchange it with other group.

The deadline of our part of project is 19th of May 23:59.