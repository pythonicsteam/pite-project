***
Last modification: April 12, 2015
# Code guidelines
***

## Introduction
This document's purpose is to gather and standarize code writing rules throughout project.
Presented code convention might change over time and if so, all the code is to be adjusted to the latest convention version.

## Whitespaces
For indentation use tabs, not spacess.
Leave no unnecessary whitespaces at the end of lines.
Put spaces on both sides of operators:
```
#!python
five = 5
polynomialValue = 15 * x + 8.6 * x * x + 12.5
```

Also, put a space after a comma:
```
#!python
integral = calledFunction(five, polynomialValue, name)
```

Do not put whitespace between if statement condition and colon or between function name and left braket.
Also, do not put them after left bracket and before right.

Distinct statements write in separate lines and add additional empty line to separate blocks of code that do different things.

## Naming scheme
All the names used in project should be written in English, should be descriptive, self explanatory and reasonably long.
Do not use veryLongNameForAVariableThatDoesStuff if it is not really necessary.

Every distinct, separable part of the project is to be contained within separate directory to create a Python module.
Names of the modules should be short, preferably one-worded, written in CamelCase style: Core, Graphics, DatabaseConnection.

Classes names might be longer and they should also be written in CamelCase.
They should be nouns, descriptive enough to get the idea of class functionality scope.
Every class should be in it's own file and there should be no other functions there (and also no 'free' code).
The only exception migth be code contained in `if __name__ == "__main__":` clause.

All the functions are to be named using camelCase, starting with lowercase letter.
Function arguments should be named accordingly to their purpose, with the same scheme as local variables. 
To define private function inside a module use underscore prefix in it's name. Like:
```
#!python
def _onePrivateFunction(argument):
```

As for variables: local variables and function arguments should be named using camelCase with first letter in lower case.
All local variables should be defined in the begining of the scope that will use them.
This might come with an exception of control variables - but only if the variable is not to be used outside loop.

Fields of classes shall start with the `__` prefix to ensure mangling their names and to make distincition between fields and local variables very easy. Example:
```
#!python
__fieldName
```
Every field of a class must be initialized in the constructor. For every field that is to be accessed from outside the class create getter and/or setter function. 
Name it getFieldName or setFieldName, skipping `__` prefix.

## Testing
Develop tests for your module in Test subfolder, creating more subfolders to reflect project structure.
Test as much as you can. Name testing classes like tested classes with prefix `Test`.

## Exceptions
Use exception handling: raise and catch.
Handle thougthfuly: if having a function that divides two numbers, use it in try-except block to catch (and handle) possible ValueErrors.
Do not handle those exceptions inside the function - at least not if there is no way to propagate error.
If the function returns status of performed operation (in ex. as a boolean) handle (expected) exceptions inside and return proper status.

## Comments and documentation
We will use Doxygen to generate documentation - in order to do that properly use comments starting with \# character as showed on [ Doxygen ](http://www.stack.nl/ dimitri/doxygen/manual/docblocks.html#pythonblocks)'s website.
Always document APIs that are used between modules.
That means documenting all not-private module functions and classes.

To disable code use string literals that span between triple quotation mark markups. Like this:
```
#!python
"""This is a disabled part of code"""
```
Try not to leave disabled code when you commit.

For descriptive comments use \# comments, if possible, at the end of the line that they're describing.
